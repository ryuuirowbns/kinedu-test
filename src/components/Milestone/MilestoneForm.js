import React from "react";


class MilestoneForm extends React.Component {


    render() {
        return (
            <div id={'form'+this.props.id} className="milestone-form" >
                <div className="science-fact">
                   <p>{this.props.scienceFact}</p>
                    <p className="source-data"><span>Source data:</span> {this.props.sourceData}</p>
                </div>
                <div className="milestone-description left">
                    <p>{this.props.description}</p>
                </div>
                <div className="milestone-answers right">
                    <div>
                        <input
                            checked={ localStorage.getItem('answer-'+this.props.id) === 'completed' ? localStorage.getItem('answer-'+this.props.id) : '' }
                            type="radio"
                            id={'yesanswer-'+this.props.id}
                            name={'answer-'+this.props.id}
                            value="completed"
                            onClick={(e)=>this.props.saveAnswer(e)}
                        />
                        <label for={'yesanswer-'+this.props.id}>My baby has completed.</label>
                    </div>
                    <div>
                        <input
                            checked={ localStorage.getItem('answer-'+this.props.id) === 'uncompleted' ? localStorage.getItem('answer-'+this.props.id) : '' }
                            type="radio"
                            id={'noanswer-'+this.props.id}
                            name={'answer-'+this.props.id}
                            value="uncompleted"
                            onClick={(e)=>this.props.saveAnswer(e)}
                        />
                        <label for={'noanswer-'+this.props.id}>My baby has not completed.</label>
                    </div>
                </div>
            </div>
        );
    }
}

export default MilestoneForm;
