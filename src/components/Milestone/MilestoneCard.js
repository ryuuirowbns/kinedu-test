import React from "react";
import ok from './ok.svg';


class MilestoneCard extends React.Component {
    constructor(props) {
        super(props);
        this.statuses = {
            notanswered: 'Not Answered',
            completed: 'Completed',
            uncompleted: 'Uncompleted'
        }

        this.handleAnswerStatus = this.handleAnswerStatus.bind(this);
    }

    handleAnswerStatus() {
        var status = localStorage.getItem('answer-'+this.props.id) ? localStorage.getItem('answer-'+this.props.id) : 'notanswered';

        return  this.statuses[status];
    }

    render() {
        return (
            <div className="milestone-clickable" onClick={() => this.props.visibility('form'+this.props.id)}>
                <div className="left">
                    <div className="milestone-title"><span>{this.props.title}</span></div>
                    <div className="milestone-age"><span>Usually achieved by: {this.props.age} months</span></div>
                </div>
                <div className="right">
                    <div className={'milestone-button desktop ' + ( localStorage.getItem('answer-'+this.props.id) ? localStorage.getItem('answer-'+this.props.id) : 'notanswered' )}>
                        {this.handleAnswerStatus()}
                    </div>
                    <div className='milestone-status mobile '>
                        <div className={'circle '+ ( localStorage.getItem('answer-'+this.props.id) ? localStorage.getItem('answer-'+this.props.id) : 'notanswered' )}>
                            <img src={ok} alt="" />
                        </div>
                        <span>{this.handleAnswerStatus()}</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default MilestoneCard;
