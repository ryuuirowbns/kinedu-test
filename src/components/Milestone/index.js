import React from "react";
import MilestoneCard from "./MilestoneCard.js"
import MilestoneForm from "./MilestoneForm.js"


class Milestone extends React.Component {
    constructor(props) {
        super(props);


         this.handleVisibility = this.handleVisibility.bind(this);
         this.saveAnswer = this.saveAnswer.bind(this);
    }

    handleVisibility(destination) {
        var targetId = destination;
        var element = document.querySelector('#'+targetId);

        if(element.classList.contains('show')){
            element.classList.remove('show');
        } else {
            element.classList.add('show');
        }
    }

    saveAnswer(e) {
        localStorage.setItem(e.target.name, e.target.value);
        this.forceUpdate();
    }

    render() {
        return (
            <div>
                {this.props.milestonesList.map(item => (
                    <div className="milestone">
                        <MilestoneCard
                            title={item.title}
                            age={item.age}
                            visibility={this.handleVisibility}
                            id={item.skill_id+'-'+item.id}
                        />
                        <MilestoneForm
                            scienceFact={item.science_fact}
                            sourceData={item.source_data}
                            description={item.description}
                            id={item.skill_id+'-'+item.id}
                            saveAnswer={this.saveAnswer}
                        />
                        <div className="clear"></div>
                    </div>
                ))}
            </div>
        );
    }
}

export default Milestone;
