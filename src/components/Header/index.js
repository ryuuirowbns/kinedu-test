import React from "react";

class Header extends React.Component {

    render() {
        return (
            <div id="head" className="head physical">
               <h3>
                   Areas
               </h3>
                <div className="areas-buttons">
                    <div id="button-physical" className="areas-button left-button areas-active active-physical" onClick={()=>this.props.getInfo('physical')}>
                        <span>Physical</span>
                    </div>
                    <div id="button-social" data-service="social" className="areas-button right-button" onClick={()=>this.props.getInfo('social')}>
                        <span >Social & emotional</span>
                    </div>
                </div>
                <hr/>
                <div className="head-description">
                    <h2>Skill: {this.props.title}</h2>
                    <p>
                        {this.props.description}
                    </p>
                </div>
            </div>
        );
    }
}

export default Header;
