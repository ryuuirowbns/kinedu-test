import React from "react";
import Header from "./components/Header";
import Milestone from "./components/Milestone";
import './general.css';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            skillTitle: '',
            skillDescription: '',
            milestonesArray: [],
            button: 'Next'
        }

         this.getData = this.getData.bind(this);
         this.servicesSelector = this.servicesSelector.bind(this);
         this.nextService = this.nextService.bind(this);
    }

    componentDidMount() {
        this.getData('physical');
    }

    getData(service) {
        const services = {
            physical: '23',
            social: '2'
        };
        const token = 'Token token=5105f4358e45f6f98057a654c882b7742c3ac5241c81a706acc48c84f8acde9f8a344993ac42369627ae9f2caf1eed42ff1be9562fe2167c9c80908e76e95c49';

        fetch(`https://staging.kinedu.com/api/v3/skills/${services[service]}/milestones`, {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': token},
        }).then(response => response.json())
        .then(resp => {
            console.log(resp.data.skill.milestones)
            this.setState({skillTitle: resp.data.skill.title, skillDescription: resp.data.skill.description, milestonesArray: resp.data.skill.milestones});
        })
        .catch(err => console.log(err));
    }

    handleServicesChange(service) {
        var serviceInverse = service === 'physical' ? 'social' : 'physical';
        var element = document.querySelector('#button-'+service);
        var elementInverse = document.querySelector('#button-'+serviceInverse);

        if(!element.classList.contains('active-'+service)) {
            this.getData(service);
            document.querySelector('#head').classList.remove(serviceInverse);
            document.querySelector('#head').classList.add(service);
            elementInverse.classList.remove('areas-active');
            elementInverse.classList.remove('active-'+serviceInverse);
            element.classList.add('areas-active');
            element.classList.add('active-'+service);
        }
    }

    servicesSelector(service) {
        this.handleServicesChange(service);
    }

    nextService() {
        this.handleServicesChange('social');
        this.setState({button: 'Finish assessment'})
    }

    render() {
        return (
            <div className="App">
                <div className="container">
                    <Header
                        title={this.state.skillTitle}
                        description={this.state.skillDescription}
                        getInfo={this.servicesSelector}
                    />
                    <div className="content-container">
                        <Milestone
                            milestonesList={this.state.milestonesArray}
                        />
                        <div className="button" onClick={()=>this.nextService()}>{this.state.button}</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
