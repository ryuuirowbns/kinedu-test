# Correr proyecto

clonar repositorio

## Ejecutar de manera local

cd nombre_carpeta

### `npm install`

instalará dependencias

### `npm start`

Correrá la carpeta en el modo .\
abrir [http://localhost:3000](http://localhost:3000) para verla en el navegador.

### `npm run build`

Compilará el proyecto para subirlo a un servidor en modo productivo.

### Notas

Ya que la prueba no mencionaba cómo se debían responder los eventos me di la libertad de añadir radiobuttons con un elemento que abre al dar clic en cada milestone.

Dado que la fuente Próxima es de paga y no cuento con ella utilicé Lato que es la más parecida a esta.

Probablemente se preguntarán el motivo de no utilizar refs para alcanzar los elementos clicables, 
esto es debido a que no me gusta sobrecargar el estado y los refs sólo deberían utilizarse en casos muy necesarios.

Por otro lado utilicé la forma de clases en vez de la programación funcional ya que para mi es más cómoda
y permite utilizar patrones de diseño de manera tradicional, también se utilizar la forma funcional y los hooks
auqnue siento que aún falta un poco para que realmente puedan ser utilizados en su totalidad en proyectos avanzados